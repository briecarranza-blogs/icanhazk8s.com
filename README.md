# icanhazk8s.com

> The source for the Web site at https://icanhazk8s.com

## Details

## Preview
The dev instance is available at [https://briecarranza-blogs.gitlab.io/icanhazk8s.com/](https://briecarranza-blogs.gitlab.io/icanhazk8s.com/) and is served on GitLab Pages. 

The production instance is available at [https://icanhazk8s.com](https://icanhazk8s.com) and is served via Firebase. 

This is a static site generated via Hugo. 
