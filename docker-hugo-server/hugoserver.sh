# !/bin/bash
#
# File: hugoServer
#
# Purpose: Run the Hugo container as a local server
#

docker stop hugo-server
docker rm   hugo-server
docker run -p 1313:1313 -v /home/brie/repositories/gitlab/brie/icanhazk8s.com/src/icanhazk8s.com/:/site  -v /tmp/hugo/test:/site/public    -e VIRTUAL_HOST="${1}" --name hugo-server devopsdays/docker-hugo-server:v0.30.2
