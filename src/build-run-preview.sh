#!/bin/bash

BUILD_NAME=$1

rm -rf  /tmp/omgomgomgomgogmomg/

docker build -t registry.gitlab.com/brie/icanhazk8s.com/hugo:${BUILD_NAME} .

docker run  -v /tmp/omgomgomgomgogmomg:/usr/share/hugo/public   registry.gitlab.com/brie/icanhazk8s.com/hugo:${BUILD_NAME}

google-chrome /tmp/omgomgomgomgogmomg/index.html
