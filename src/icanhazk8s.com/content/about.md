---
title: "About"
date: 2020-11-08T17:49:56-05:00
draft: false
---

I decided to get serious about learning Kubernetes in fall 2020. I often find writing about a topic to be a useful part of the learning process. This site collects the posts and notes I wrote into one place alongside links and tools I found useful. I hope this information is useful for you on your journey towards a better understanding of Kubernetes. 

--


Brie
