---
title: "Fundamental K8s Terms, Part 00: Pod, Node, Cluster"
date: 2020-11-03T01:04:59Z
tags: ['k8s', 'kubernetes', 'node', 'pod', 'cluster', 'fundamental', 'terms', 'vocabulary']
draft: false
---

Learning Kubernetes involves learning a whole set of new terms. Among the most essential are **pod**, **node** and **cluster**. <!--more-->

The official Kubernetes documentation has a [Standardized Glossary](https://kubernetes.io/docs/reference/glossary/) with several dozens of of k8s terms. The Standardized Glossary is filterable. Filtering to show only **Fundamental** terms leaves you with just under 60 terms:

  - [https://kubernetes.io/docs/reference/glossary/?fundamental=true](https://kubernetes.io/docs/reference/glossary/?fundamental=true)

 In the **Fundamental K8s Terms** series, we will select a few terms at a time to learn. Reading these blog posts will introduce these terms in the order in which I learned them. This is merely a suggested order based on my needs. I am documenting my experience in case you find it helpful! Some of the **Fundamental** terms are not unique to Kubernetes. Here are the  **Fundamental** terms that I will not be covering in the **Fundamental K8s Terms** series:

  - cgroup (control group)
  - Container
  - Docker
  - Image

I have linked to suggested articles in case you need to brush up on some of these terms. I would _strongly_ encourage you to make sure you are comfortable with each of the four terms above before proceeding. 
 
## Fundamental K8s Terms 
The terms we will cover in the first installment of this series are:

  - cluster
  - node
  - pod
  
Chances are that one or more of these terms is familiar to you already. Let's understand what these terms mean in a Kubernetes-context.   
  
### cluster
A **cluster** is a set of nodes. Each cluster has at least one **node**. 

> When you deploy Kubernetes, you get a cluster.

[source](https://kubernetes.io/docs/concepts/overview/components/)

### node
A **node** is a worker machine in Kubernetes. Pods run on a **node**. A **node** can be virtual machine or a physical machine. 

  - [ ] Read [Nodes](https://kubernetes.io/docs/concepts/architecture/nodes/) from the **Cluster Architecture** section of the Kubernetes docs. 

### pod
A **pod** is a set of tightly-coupled containers. A **pod** is a group of [one or more containers with shared storage and network resources](https://kubernetes.io/docs/concepts/workloads/pods/). All containers in a **pod** are co-located (on the same node). 

Kubernetes by example: [pods](https://kubernetesbyexample.com/pods/)

### How are clusters, nodes and pods related?
A Kubernetes **cluster** consists of a set of worker machines; each worker machine is called a **node**. Each node hosts one or more **pods**. 

  - [Kubernetes 101: Pods, Nodes, Containers, and Clusters](https://medium.com/google-cloud/kubernetes-101-pods-nodes-containers-and-clusters-c1509e409e16)
  - [Understanding the Relationships Between Kubernetes Clusters, Nodes, and Pods](https://blog.newrelic.com/engineering/kubernetes-clusters-nodes-and-pods/)

