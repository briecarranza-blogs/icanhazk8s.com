
---
title: "On Namespaces"
date: 2020-12-19T00:30:15-05:00
tags: ['k8s', 'kubernetes', 'namespaces']
draft: false
summary: "What is a namespace and when to use one"
---

Namespaces permit separating a single Kubernetes cluster into virtual clusters. A simple use case is separate namespaces for `development` and `production`. <!--more-->

Resource  names must be unique within a namespace but needn't be unique across namespaces. 

**Q**: When should you use namespaces?
**A**: When you need them. 

It may be possible to us `labels` to distinguish resources within the same namespace. Avoid creating namespaces that start with `kube-`; leave those for Kubernetes. The docs say start using namespaces when they start providing value. 

Phases of a namespace are: `Active` and `Terminating`. This is [based on whether](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/architecture/namespaces.md#phases) the namespace has a `ObjectMeta.DeletionTimestamp`. 

Cross Namespace communication [is possible](https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-organizing-with-namespaces):

> Namespaces are “hidden” from each other, but they are not fully isolated by default.

[Network policies](https://ahmet.im/blog/kubernetes-network-policy/) should be used  in order to isolate namespaces. 

## Default Namespaces

```
default                Active   57d
kube-node-lease        Active   57d
kube-public            Active   57d
kube-system            Active   57d
```

## When are namespaces useful?

  - When you want to share a cluster. 

The [Namespaces walkthrough](https://kubernetes.io/docs/tasks/administer-cluster/namespaces-walkthrough/) has some more useful notes on this. Namespaces provide:

> A mechanism to attach authorization and policy to a subsection of the cluster

  - [Share a Cluster with namespaces](https://kubernetes.io/docs/tasks/administer-cluster/namespaces/)

You can have `development` and `production` namespaces in the same cluster. The dev and ops teams can have separate Pods, Services and Deployments. (See note above about cross namespace communication.)

The **Namespace granularity** section of Google's [Organizing with Namespaces](https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-organizing-with-namespaces) has a nice quick response on this note. 

## Using namespaces

With various `kubectl` subcommands, you use `--namespace=foo` to target the appropriate namespace. 

Try `kubectx` + `kubens`, the power tools for `kubectl`: [ahmetb/kubectx](https://github.com/ahmetb/kubectx). These tools let you switch between contexts and namespaces respectively. 

## Docs
  - Working With Objects: [Namespaces](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
  - [Kubernetes namespaces by example](https://kubernetesbyexample.com/ns/)
  - Kubernetes Best Practices: [Organizing with Namespaces](https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-organizing-with-namespaces)

