---
title: "Useful Links"
date: 2020-11-08T17:54:32-05:00
draft: false
---

## Essentials
  - [awesome-kubernetes](https://github.com/ramitsurana/awesome-kubernetes) - The premier awesome-kubernetes list. 

## Tutorials

## Podcasts
Podcasts to subscribe to and particular episodes to listen to. 
### Subscribe to this podcast.

  - [Kubernetes Podcast from Google](https://kubernetespodcast.com/)

### Listen to this episode. 

  - [Minikube Redux, with Thomas Strömberg](https://kubernetespodcast.com/episode/115-minikube-redux/) from Kubernetes Podcast from Google on August 4, 2020


## Videos
  - The videos for [KubeCon 2019](https://www.youtube.com/playlist?list=PLj6h78yzYM2NDs-iu8WU5fMxINxHXlien) are available as a YouTube playlist. 




  > Last modified: 2020-12-December-19